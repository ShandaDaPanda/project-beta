from django.urls import path
from .views import api_service_details, api_list_services, api_list_technicians, api_technician_details

urlpatterns = [
    path("services/", api_list_services, name="api_list_services"),
    path("services/<int:pk>/", api_service_details, name="api_service_details"),
    path("technicians/", api_list_technicians, name="api_list_technicians"),
    path("technicians/<int:pk>/", api_technician_details, name="api_technician_details"),
]
