from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import Service, AutomobileVO, Technician
from .encoders import ServicesEncoder, TechnicianEncoder


@require_http_methods(["DELETE", "GET", "PUT"])
def api_service_details(request, pk):

    if request.method == "GET":
        try:
            service = Service.objects.get(id=pk)
            return JsonResponse(
                service,
                encoder=ServicesEncoder,
                safe=False,
            )
        except Service.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            service = Service.objects.get(id=pk)
            service.delete()
            return JsonResponse(
                service,
                encoder=ServicesEncoder,
                safe=False,
            )
        except Service.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            service = Service.objects.get(id=pk)

            props = [   "vin",
                        "status",
                        "customer_name",
                        "service_datetime",
                        "technician",
                        "reason"
                        "vip_status",
                        "completed_status",
                    ]
            for prop in props:
                if prop in content:
                    setattr(service, prop, content[prop])
            service.save()
            return JsonResponse(
                service,
                encoder=ServicesEncoder,
                safe=False,
            )
        except Service.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 400
            return response

@require_http_methods(["GET", "POST"])
def api_list_services(request):

    if request.method == "GET":
        services = Service.objects.all()
        return JsonResponse(
            {"services": services},
            encoder=ServicesEncoder
        )
    else:

        try:
            content = json.loads(request.body)

            technician = content["technician"]
            technician = Technician.objects.get(id=technician)
            content["technician"] = technician

            service = Service.objects.create(**content)
            return JsonResponse(
                service,
                encoder=ServicesEncoder,
                safe=False,
            )
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Vin"},
                status = 400,
            )


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Could not add technician"},
                status = 400,
            )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_technician_details(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            technician = Technician.objects.get(id=pk)
            technician.delete()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.get(id=pk)
            props = ["name", "employee_id"]
            for prop in props:
                if prop in content:
                    setattr(technician, prop, content[prop])
            technician.save()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
