from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200, unique = True)


class Salesperson(models.Model):
    name = models.CharField(max_length=100)
    employeenumber = models.CharField(max_length=100)


class Customer(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=200)
    phone = models.CharField(max_length=10)


class Sales(models.Model):
    price = models.SmallIntegerField()
    employee = models.ForeignKey(
        Salesperson,
        related_name="sales",
        on_delete=models.PROTECT,
    )
    customer = models.ForeignKey(
        Customer,
        related_name="sales",
        on_delete=models.CASCADE,
    )

    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sales",
        on_delete=models.CASCADE,
    )


    def get_api_url(self):
        return reverse("api_sale", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.employee} - {self.customer}"

    class Meta:
        ordering = ("employee", "customer")
