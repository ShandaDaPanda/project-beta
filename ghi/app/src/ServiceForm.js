import React, { useState, useEffect } from 'react';
import DateTimePicker from 'react-datetime-picker';

function ServiceForm() {
  const [technicians, setTechnicians] = useState([])
  const [formData, setFormData] = useState({
    vin: '',
    customer_name: '',
    service_datetime: '2023-02-03',
    reason: '',
    technician: '',
    vip_status: "True",
    completed_status: "False"
  })


  const getData = async () => {
    const url = 'http://localhost:8080/api/technicians/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  }

  useEffect(() => {
    getData();
  }, []);

  const [value, onChange] = useState(new Date());

  const handleSubmit = async (event) => {
    event.preventDefault();

    const technicianUrl = 'http://localhost:8080/api/services/';

    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    console.log(formData)

    const response = await fetch(technicianUrl, fetchConfig);

    if (response.ok) {
      setFormData({
        vin: '',
        customer_name: '',
        service_datetime: '',
        reason: '',
        technician: '',
      });
      window.location.replace('/services')
    }
  }

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,

      [inputName]: value
    });
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a new Service</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
              <textarea onChange={handleFormChange} value={formData.vin} placeholder="vin" className="form-control" id="vin" rows="3" name="vin"></textarea>
              <label htmlFor="description">Vin</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.customer_name} placeholder="customer" required type="text" name="customer_name" id="customer_name" className="form-control" />
              <label htmlFor="name">Customer Name</label>
            </div>
            <div className="mb-3">
              <label htmlFor="description">Service Date & Time</label>
              <DateTimePicker onChange={onChange} value={value} />
            </div>
            <div className="form-floating mb-3">
              <textarea onChange={handleFormChange} value={formData.reason} placeholder="reason" className="form-control" id="reason" rows="3" name="reason"></textarea>
              <label htmlFor="description">Reason</label>
            </div>

            <div className="mb-3">
              <select onChange={handleFormChange} value={formData.technician.name} required name="technician" id="technician" className="form-select">
                <option value="">Choose a technician</option>
                {technicians.map(technician => {
                  return (
                    <option key={technician.id} value={technician.id}>{technician.name}</option>
                  )
                })}
              </select>
            </div>

            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ServiceForm;
