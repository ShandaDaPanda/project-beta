import React, { useState, useEffect } from 'react';

function AutomobilesForm() {
  const [models, setModels] = useState([])
  const [formData, setFormData] = useState({
    color: '',
    year: '',
    vin: '',
    model_id: '',
  })

  const getData = async () => {
    const url = 'http://localhost:8100/api/models/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  }

  useEffect(() => {
    getData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const modelUrl = 'http://localhost:8100/api/automobiles/';
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(modelUrl, fetchConfig);

    if (response.ok) {
      setFormData({
        color: '',
        year: '',
        vin: '',
        model_id: '',
      });
      window.location.replace('/automobiles')
    }
  }

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value
    });
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new Automobile</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="name">Color</label>
            </div>
            <div className="form-floating mb-3">
              <textarea onChange={handleFormChange} value={formData.year} placeholder="Year" className="form-control" id="year" rows="3" name="year"></textarea>
              <label htmlFor="description">Year</label>
            </div>
            <div className="form-floating mb-3">
              <textarea onChange={handleFormChange} value={formData.vin} placeholder="Vin" className="form-control" id="vin" rows="3" name="vin"></textarea>
              <label htmlFor="description">Vin</label>
            </div>
            <div className="mb-3">
              <select onChange={handleFormChange} value={formData.model_id.name} required name="model_id" id="model_id" className="form-select">
                <option value="">Choose a model</option>
                {models.map(model => {
                  return (
                    <option key={model.id} value={model.id}>{model.name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default AutomobilesForm;
