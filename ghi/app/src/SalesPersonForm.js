import React, { useState, useEffect } from 'react';

function SalespersonForm() {
  const [formData, setFormData] = useState({
    name: '',
    employeenumber: '',
  })


  const handleSubmit = async (event) => {
    event.preventDefault();

    const salespersonUrl = 'http://localhost:8090/api/salespersons/';

    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(salespersonUrl, fetchConfig);

    if (response.ok) {
      setFormData({
        name: '',
        employeenumber: '',
      });
      window.location.replace('/salespersons')
    }
  }

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,

      [inputName]: value
    });
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new Salesperson</h1>
          <form onSubmit={handleSubmit} id="create-salespersons-form">
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.employeenumber} placeholder="employeeID" required type="text" name="employeenumber" id="employeenumber" className="form-control" />
              <label htmlFor="name">EmployeeID</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default SalespersonForm;
