import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function ManufacturersList() {
  const [manufacturers, setManufacturers] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8100/api/manufacturers/');

    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers)
    }
  }
  useEffect(() => {
    getData()
  }, [])

  const deleteManufacturer = async (id) => {
    const response = await fetch(`http://localhost:8100/api/manufacturers/${id}/`, {
      method: 'DELETE',
      mode: "cors",
      headers: {
        "Content-Type": "application/json"
      }
    })
    const data = await response.json()
    setManufacturers(
      manufacturers.filter((manufacturer) => {
        return manufacturer.id !== id;
      })
    )
  }

  return (
    <>
      <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
        <Link to="/manufacturers/new" className="btn btn-primary btn-lg px-4 gap-3">Add a Manufacturer</Link>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Make</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {manufacturers.map(manufacturer => {
            return (
              <tr key={manufacturer.id}>
                <td>{manufacturer.name}</td>
                <td>
                  <button onClick={() => deleteManufacturer(manufacturer.id)}>Delete</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default ManufacturersList;
