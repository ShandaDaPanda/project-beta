import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function ServiceApptList() {
  const [services, setServices] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8080/api/services/');
    if (response.ok) {
      const data = await response.json();
      setServices(data.services);
    }
  }

  useEffect(() => {
    getData();
  }, [])

  const cancelServiceAppt = async (id) => {
    const response = await fetch(`http://localhost:8080/api/services/${id}/`, {
      method: 'PUT',
      mode: "cors",
      headers: {
        "Content-Type": "application/json"
      }
    })
    const data = await response.json()
    setServices(
      services.filter((service) => {
        return service.id !== service;
      })
    )
  }

  const completeServiceAppt = async (id) => {
    const response = await fetch(`http://localhost:8080/api/services/${id}/`, {
      method: 'PUT',
      mode: "cors",
      headers: {
        "Content-Type": "application/json"
      }
    })
    const data = await response.json()
    setServices(
      services.filter((service) => {
        return service.id !== service;
      })
    )
  }

  return (
    <>
      <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
        <Link to="/services/new" className="btn btn-primary btn-lg px-4 gap-3">New Service Appt</Link>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Customer Name</th>
            <th>Service Date & Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>VIP Status</th>
            <th>Completed Status</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {services.map(service => {
            return (
              <tr key={service.id}>
                <td>{service.vin}</td>
                <td>{service.customer_name}</td>
                <td>{service.service_datetime}</td>
                <td>{service.technician.name}</td>
                <td>{service.reason}</td>
                <td>{service.vip_status}</td>
                <td>{service.completed_status}</td>
                <td>
                  <button onClick={() => cancelServiceAppt(service.id)}>Cancel</button>
                  <button onClick={() => completeServiceAppt(service.id)}>Completed</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default ServiceApptList;
